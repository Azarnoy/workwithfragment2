package com.dmitriy.azarenko.workwithfragment2;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;


//почему replace а не add
// кликать можно так или ставить кнопку
// как создать массив в АС
// как связать массив с кликами









public class MainActivity extends AppCompatActivity {









    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        FirstFragment firstFragment = FirstFragment.newInstance();
        SecondFragment secondFragment = SecondFragment.newInstance();
        ThirdFragment thirdFragment = ThirdFragment.newInstance();
        FourthFragment fourthFragment = FourthFragment.newInstance();

        ft.add(R.id.First_frag, firstFragment,FirstFragment.class.getSimpleName());
        ft.add(R.id.Second_frag, secondFragment,SecondFragment.class.getSimpleName());
        ft.add(R.id.Third_frag, thirdFragment,ThirdFragment.class.getSimpleName());
        ft.add(R.id.Fourth_frag, fourthFragment,FourthFragment.class.getSimpleName());
        ft.commit();






    }
}
