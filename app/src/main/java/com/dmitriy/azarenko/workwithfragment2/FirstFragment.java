package com.dmitriy.azarenko.workwithfragment2;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class FirstFragment extends Fragment {


    public FirstFragment() {
        // Required empty public constructor
    }


    public static FirstFragment newInstance() {
        FirstFragment fragment = new FirstFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_first, null);

        view.setBackgroundColor(Color.parseColor("#76FF03"));

        return view;
    }


}
